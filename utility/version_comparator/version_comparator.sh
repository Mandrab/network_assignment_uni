#!/bin/bash

# check input num
if (($# != 2)); then 
	echo wrong number of input: expected 2, passed $#;
	exit 1;
fi

echo -n message to simulate:    
read msg_num

echo -n loss prob:    
read loss

echo -n corruption prob:
read corr

echo -n avg time:
read avg_t

echo -n windows size:
read wnds

echo -n retrasmission time:
read ret_t

echo -n trace:
read trace

echo -n seed:
read seed

$1 << ANSWERS > output_1.txt
$msg_num 
$loss 
$corr 
$avg_t 
$wnds 
$ret_t 
$trace 
$seed
ANSWERS

$2 << ANSWERS > output_2.txt
$msg_num 
$loss 
$corr 
$avg_t 
$wnds 
$ret_t 
$trace 
$seed
ANSWERS

echo $'\n\n-------------------------------HERE COMES THE DIFFERENCES-------------------------------\n\n'
diff output_1.txt output_2.txt

rm -f output_1.txt output_2.txt