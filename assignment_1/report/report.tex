%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

% !TeX spellcheck = it
\documentclass{article}

\usepackage{siunitx} % Provides the \SI{}{} and \si{} command for typesetting SI units
\usepackage{graphicx} % Required for the inclusion of images
\usepackage{pdfpages}
\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements
\usepackage[export]{adjustbox}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[margin=1.5in]{geometry}
\usepackage[italian]{babel}

\setlength\parindent{0pt} % Removes all indentation from paragraphs
\setcounter{secnumdepth}{0}% disables section numbering

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)
\newenvironment{simil_console_font}{\fontfamily{pcr}\selectfont}{\par}

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Programmazione di Reti \\ Assignment 1} % Title

\author{Paolo Baldini \and Marco Meluzzi} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\newpage

\tableofcontents

\newpage

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Task 1}
\begin{itemize}
    \item Dopo aver avviato Wireshark e selezionato l’interfaccia \textit{Loopback} per 
    la cattura del traffico sul localhost, è stato impostato il filtro per catturare i 
    soli pacchetti di una connessione UDP, specificando la porta sulla quale 
    intercettarli (nel nostro caso la porta numero 1234).
    \item Sono state quindi avviate due istanze del terminale sulle quali far girare il 
    lato client e il lato server della funzionalità CLIENT/SERVER MODEL dell’applicativo 
    \textbf{nc}.
    \begin{figure}[!htb] \centering
        \includegraphics[width=.994\textwidth]{res/wireshark_screen.pdf}
        \caption{\textit{Traffico generato dalla funzione CLIENT/SERVER MODEL di nc.}}
    \end{figure}
    \newline
    Il \textit{lato server} è stato istanziato con il seguente comando:
    \begin{center}
        {\fontfamily{cmtt}\selectfont nc -lu 1234}
    \end{center}
    dove:	
    \begin{description}
        \item {\fontfamily{cmtt}\selectfont -l}: specifica che \textbf{nc} si deve mettere in 
        ascolto delle connessioni in ingresso;
        \item {\fontfamily{cmtt}\selectfont -u}: specifica una connessione di tipo UDP;
        \item {\fontfamily{cmtt}\selectfont 1234}: è la porta sulla quale è collegato.
    \end{description}
    Il \textit{lato client} è stato invece istanziato nel modo seguente:
    \begin{center}
       {\fontfamily{cmtt}\selectfont nc -u 127.0.0.1 1234}
    \end{center}
    dove:
    \begin{description}
        \item {\fontfamily{cmtt}\selectfont -u}: specifica una connessione di tipo UDP;
        \item {\fontfamily{cmtt}\selectfont 127.0.0.1}: specifica l’indirizzo IP della 
        macchina a cui connettersi (nel nostro caso corrisponde all’indirizzo del 
        localhost);
        \item {\fontfamily{cmtt}\selectfont 1234}: è la porta verso cui invierà i 
        propri messaggi.
    \end{description}
\end{itemize}
Avviata la comunicazione tra i due processi, la funzionalità presa in esame consente al 
processo client di inviare messaggi al processo server, che li visualizzerà a video. 
Tuttavia, una volta che la connessione è stata stabilita, \textbf{nc} non distingue più tra 
client e server, pertanto è anche possibile inviare messaggi dal server verso il client.
\newpage

%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Task 2}
L’approccio risolutivo adottato vede l’entità che rappresenta il
processo server in ascolto continuo sulla porta e sull’indirizzo IP
selezionati all’avvio e l’entità client che attende in ingresso
dall’utente i messaggi da inviare all’indirizzo IP e alla porta
specificati (che devono corrispondere a quelli indicati per il server).
Quando vuole terminare la comunicazione, il client invierà un
messaggio di uscita al server, il quale risponderà a sua volta con un
messaggio ad indicare che il client può effettivamente terminare. Il
server, invece, continua a rimanere in ascolto. Diversamente da
quanto osservato su \textbf{netcat}, si è volutamente lasciata la possibilità
di istanziare più client contemporaneamente, allo scopo di
argomentare il Task 3 sulla base di quanto implementato. Di
conseguenza, il server invierà la stringa all’ultima istanza del client
da cui ha ricevuto un messaggio. Infine, come scelta
implementativa, si è deciso di ignorare i messaggi inviati dal server 
se un’istanza del client non si sia prima connessa (i.e. non abbia
ancora inviato un messaggio al server).
\begin{figure}[!htb] \centering
    \includegraphics[width=\textwidth]{res/task2_running.pdf}
    \caption{\textit{Test di funzionamento}}
\end{figure}

\paragraph{\fontfamily{cmtt}\selectfont UDPserver.c\\}
L’applicativo prende in ingresso il valore del proprio indirizzo IP e
della porta sulla quale mettersi in ascolto. Per prima cosa crea una
socket specificando il dominio di comunicazione ({\fontfamily{cmtt}\selectfont AF\_INET}, la
famiglia di indirizzi IPv4), il tipo di pacchetti inviati ({\fontfamily{cmtt}\selectfont SOCK\_DGRAM}) e
il protocollo (UDP). Dopo aver inizializzato la struttura contenente le
informazioni relative al server con gli opportuni valori, questa viene
associata alla socket tramite la funzione {\fontfamily{cmtt}\selectfont bind()}. Successivamente il
server si mette in attesa dei messaggi in arrivo dal client tramite la
funzione {\fontfamily{cmtt}\selectfont recvfrom()}, che tra gli altri prende come argomento
l’indirizzo della struttura contenente i dati del suddetto client. Se
quanto ricevuto rappresenta il messaggio di chiusura, procede ad
inviare un messaggio di arrivederci tramite la funzione {\fontfamily{cmtt}\selectfont sendto()},
dopodiché si rimette in ascolto per altri messaggi.

\paragraph{\fontfamily{cmtt}\selectfont UDPclient.c\\}
Il lato client prende in ingresso il valore dell’indirizzo IP e della porta
del server verso cui inviare i messaggi. Segue anche in questo caso
la creazione della socket in cui vengono specificati gli stessi
parametri della controparte server e la successiva inizializzazione
della struttura dati contenente le informazioni del server. E’
importante notare che all’atto della creazione della socket lato
client non è necessario specificarne l’indirizzo sorgente tramite la
funzione {\fontfamily{cmtt}\selectfont bind()}, infatti questo compito viene demandato al sistema
operativo. A questo punto il processo client attende input da
tastiera da parte dell’utente, che costituirà il messaggio da inviare
al server tramite la funzione {\fontfamily{cmtt}\selectfont sendto()}. Se il messaggio appena
inviato coincide con la stringa di uscita (i.e. “exit”), si mette in
attesa tramite {\fontfamily{cmtt}\selectfont recvfrom()} dell’arrivo del messaggio di arrivederci 
(i.e. “goodbye”) da parte del server, che indica al client la
possibilità di terminare l’esecuzione. Altrimenti, torna a mettersi in
ascolto di altri messaggi da inviare al server.
\newpage

%----------------------------------------------------------------------------------------
%   SECTION 3
%----------------------------------------------------------------------------------------

\section{Task 3}
Si è scelto di discutere gli scenari seguenti in riferimento a quanto
implementato nel task precedente.

\paragraph{Scenario 1}
\begin{description}
    \item{\textbf{Supposizione:}} eseguendo un’istanza del server e due del client ci
    aspettiamo un comportamento sequenziale da parte del server, in
    quanto non vi è nessuna gestione di thread/processi in questo senso né da parte
    delle librerie impiegate, né da quanto implementato, ma che i dati
    vengano elaborati nell’ordine di arrivo.
    \item{\textbf{Verifica:}} eseguendo una prova la nostra supposizione sembra
    trovare riscontro.
\end{description}

\paragraph{Scenario 2}
\begin{description}
    \item{\textbf{Supposizione:}} anche aggiungendo una sleep di qualche secondo,
    il comportamento sopra osservato non dovrebbe variare, in quanto
    il server continua a gestire la ricezione dei messaggi da parte di più
    client in maniera sequenziale, rispettando quindi il loro ordine di
    arrivo.
    \item{\textbf{Verifica:}} applicando questa modifica, si osserva infatti che il 
    server non gestisce i nuovi messaggi in arrivo prima di aver gestito i
    precedenti.
\end{description}

\paragraph{Suggerimento:} una possibile modalità di esecuzione
“concorrente” delle richieste dei client da parte del server può
essere ottenuta creando per ogni richiesta un thread o un processo
atto a soddisfarla, lasciando quindi il processo server principale in
ascolto sulla porta, libero di riceve altri messaggi in arrivo.
\newpage

\end{document}