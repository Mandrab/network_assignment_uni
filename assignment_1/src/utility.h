/****************************************************************************************

Copyright 2018 Paolo Baldini, Marco Meluzzi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

****************************************************************************************/

#ifndef UTILITY_H
#define UTILITY_H

    #define printErrAndExit(X)  do {                        \
                                    perror(X);              \
                                    exit(EXIT_FAILURE);     \
                                } while(0)
                                                                 
    #define synchronized(MUTEX, OPERATION)                  \
        do {                                                \
            pthread_mutex_t * _lp = &(MUTEX);               \
            if (pthread_mutex_lock(_lp))                    \
                printErrAndExit("Lock error");              \
            OPERATION;                                      \
            if (pthread_mutex_unlock(_lp))                  \
                printErrAndExit("Unlock error");            \
        } while (0)

    typedef enum bool {false, true} boolean;

    void printData(char *str, size_t numBytes);

#endif
