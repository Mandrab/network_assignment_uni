/****************************************************************************************

Copyright 2018 Paolo Baldini, Marco Meluzzi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

****************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "utility.h"

#define MAX_BUF_SIZE 1024                                               /* maximum size of UDP messages */
#define NUM_ARGS 3                                                      /* number of arguments expected */

void *listener(void *arg);
void *writer(void *arg);

/* GLOBAL VARIABLE */
pthread_mutex_t mutex;						                            /* mutex variable */

struct sockaddr_in clientAddr;                                          /* struct containing client address information */
socklen_t clientSize = sizeof(clientAddr);				                /* client struct size */
int sfd; 							                                    /* server socket file descriptor */

boolean clientAddressAcquired = false;                                  /* true if server has acquired at least one address to send data */

int main(int argc, char *argv[]) {
	pthread_t th;                                                       /* thread identifier */
    struct sockaddr_in serverAddr;                                      /* struct containing server address information */
	char *sAddr;                                                        /* pointer to server IP address string */
	uint16_t sPort;                                                     /* server port number */
    
	if (argc != NUM_ARGS)												/* control of correct number of elements */
		printErrAndExit("Incorrect number of arguments.\nPlease insert server IP address and port number.\n");
	
    sAddr = argv[1];                                                    /* server IP address string */
    sPort = atoi(argv[2]);                                              /* conversion to server port number */
    
	printf("IP address: %s \nPort number: %u\n", sAddr, sPort);
	
	/* socket creation */
	sfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sfd < 0)
		printErrAndExit("socket");
	
	/* server address information initialization */
	serverAddr.sin_family = AF_INET;									/* IPv4 family address */
	serverAddr.sin_port = htons(sPort);                                 /* convert to network byte order */
	serverAddr.sin_addr.s_addr = inet_addr(sAddr);                      /* bind to sAddr host */
	
	/* server binding */
	if (bind(sfd, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
		printErrAndExit("bind");
	
    /* mutex initialization */
	if (pthread_mutex_init(&mutex, NULL))
		printErrAndExit("Error in mutex variable initialization");
	
    /* creation of listener and writer thread */
	if (pthread_create(&th, NULL, listener, NULL) || pthread_create(&th, NULL, writer, NULL))
		printErrAndExit("Error in thread creation");
	
	pthread_exit(NULL);
	return 0;
}

void *listener(void *arg) {
	struct sockaddr_in clientAddrCopy;									/* created to avoid calling recvfrom() in mutual exclusion */
	socklen_t lastReceivedClientSize = sizeof(clientAddrCopy);			/* clientAddrCopy size to pass to recvfrom */

	size_t byteRecv; 													/* number of bytes received */
	size_t byteSent; 													/* number of bytes to be sent */
	
	char receivedData[MAX_BUF_SIZE]; 									/* data to be received */
	
	while (true) {
		/* data reception */
		byteRecv = recvfrom(sfd, receivedData, MAX_BUF_SIZE, 0, (struct sockaddr *) &clientAddrCopy, &lastReceivedClientSize);
		if (byteRecv == -1) 
            printErrAndExit("recvfrom");
		
		/* critical section */
		synchronized(mutex, {
			clientSize = lastReceivedClientSize;
			clientAddr = clientAddrCopy;								/* the address of the last client from which this server received the message */
			clientAddressAcquired = true;
		});

        /* exit message notification */
		if(!strncmp(receivedData, "exit\n", byteRecv)) {
			/* critical section */
            synchronized(mutex, clientAddressAcquired = false);			
            
			/* message sending */
			byteSent = sendto(sfd, "goodbye", sizeof("goodbye"), 0, (struct sockaddr *) &clientAddrCopy, sizeof(clientAddrCopy));
			if (byteSent == -1) 
                printErrAndExit("sendto");
		}
        
		printf("IP address: %s\tPort: %d\tMessage: ", inet_ntoa(clientAddrCopy.sin_addr), htons(clientAddrCopy.sin_port));
		printData(receivedData, byteRecv);        
    }
	
	pthread_exit(NULL);
}

void *writer(void *arg) {
	struct sockaddr_in lastClientAddr;									/* the address of the last client that connected to the server */
	boolean authorized = false;											/* true if server has acquired at least one address to send data */
	char sendData[MAX_BUF_SIZE]; 										/* data to be sent */
	
	while (true) {
		fgets(sendData, MAX_BUF_SIZE, stdin);							/* acquire message from keyboard */
		
		/* critical section */
		synchronized(mutex, {
			authorized = clientAddressAcquired ? true : false;
			lastClientAddr = clientAddr;
		});

		/* sending only if client is connected */
		if (authorized && sendto(sfd, sendData, strlen(sendData), 0, (struct sockaddr *) &lastClientAddr, sizeof(lastClientAddr)) == -1)
			printErrAndExit("sendto");
	}
	
	pthread_exit(NULL);
}
