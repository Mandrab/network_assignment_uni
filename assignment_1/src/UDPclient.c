/****************************************************************************************

Copyright 2018 Paolo Baldini, Marco Meluzzi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

****************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include "utility.h"

#define MAX_BUF_SIZE 1024                                              /* maximum size of UDP messages */
#define NUM_ARGS 3                                                     /* number of arguments expected */

void *listener(void *arg);                                             /* listener thread */
void *writer(void *arg);                                               /* writer thread */

/* GLOBAL VARIABLE */
pthread_mutex_t mutex;                                                 /* mutex variable */

struct sockaddr_in serverAddr;                                         /* struct containing server address information */
socklen_t serverSize = sizeof(serverAddr);                             /* client struct size */
int sfd;                                                               /* server socket file descriptor */

boolean shouldExit = false;                                            /* condition for listener exit */

int main(int argc, char *argv[]) {
    pthread_t th;                                                      /* thread identifier */
    char *sAddr;                                                       /* pointer to server IP address string */
    uint16_t sPort;                                                    /* server port number */
    
	if (argc != NUM_ARGS)                                              /* control of correct number of elements */
		printErrAndExit("Incorrect number of arguments.\nPlease insert server IP address and port number.\n");
	
	sAddr = argv[1];                                                   /* server IP address string */
	sPort = atoi(argv[2]);                                             /* conversion to server port number */
	
	printf("IP address: %s \nPort number: %u\n", sAddr, sPort);
	
	/* socket creation */
	sfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sfd < 0)
		printErrAndExit("socket");
	
	/* server address information initialization */
	serverAddr.sin_family = AF_INET;								   /* IPv4 family address */
	serverAddr.sin_port = htons(sPort);                                /* convert to network byte order */
	serverAddr.sin_addr.s_addr = inet_addr(sAddr);                     /* bind to sAddr host */
	
    /* mutex initialization */
	if (pthread_mutex_init(&mutex, NULL))
		printErrAndExit("Error in mutex init\n");
	
    /* creation of listener and writer thread */
	if (pthread_create(&th, NULL, listener, NULL) || pthread_create(&th, NULL, writer, NULL))
		printErrAndExit("Error in thread creation\n");
	
	pthread_exit(NULL);
	return 0;
}

void *listener(void *arg) {
	struct sockaddr_in serverAddrCopy;                                 /* created to avoid to overwrite the set address 
                                                                        * in case of receive a message from another server 
                                                                        * (should not happen) */

	socklen_t serverSizeCopy = sizeof(serverAddrCopy);                 /* serverAddrCopy size to pass to recvfrom() */
	
	char receivedData[MAX_BUF_SIZE];                                   /* data to be received */
	size_t byteRecv;                                                   /* number of bytes received */
	
	boolean stop = false;                                              /* cicle stop */
	
	while (!stop) {
		/* data reception */
		byteRecv = recvfrom(sfd, receivedData, MAX_BUF_SIZE, 0, (struct sockaddr *) &serverAddrCopy, &serverSizeCopy);
        if (byteRecv == -1) 
			printErrAndExit("recvfrom"); 

		/* critical section */
		synchronized(mutex, 
			if ((!strcmp(receivedData, "goodbye")) && shouldExit)        /* if the listener receives the goodbye message AND the writer has 
																		  * sent the exit message, the listener loop will be stopped on next
																		  * cicle. In this way, if the client receives a goodbye message
																		  * without asking for exit, the listener will not be close */

				stop = true;);                                             /* set false the loop condition */
		
		/* message printing */
		printf("IP address: %s\tPort: %d\tMessage: ", inet_ntoa(serverAddr.sin_addr), htons(serverAddrCopy.sin_port));
		printData(receivedData, byteRecv);
	}
	
	pthread_exit(NULL);
}

void *writer(void *arg) {
	size_t byteSent;                                                   /* number of bytes to be sent */
	size_t msgLen;                                                     /* length of the message to be sent */
	char sendData[MAX_BUF_SIZE];                                       /* data to be sent */
	
	while (!shouldExit) {
		fgets(sendData, MAX_BUF_SIZE, stdin);                          /* acquire message from keyboard */
		msgLen = strlen(sendData);                                     /* set length of the string */
		
        /* exit control */
		if (strcmp(sendData, "exit\n") == 0)                           /* user asking for exit */
			/* critical section */
			synchronized(mutex, shouldExit = true);					   
        
		/* message sending */
		byteSent = sendto(sfd, sendData, msgLen, 0, (struct sockaddr *) &serverAddr, sizeof(serverAddr));
		if (byteSent == -1)
			printErrAndExit("sendto");
	}
	
	pthread_exit(NULL);
}
