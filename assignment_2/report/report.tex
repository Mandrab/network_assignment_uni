%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

% !TeX spellcheck = it
\documentclass{article}

\usepackage{siunitx} % Provides the \SI{}{} and \si{} command for typesetting SI units
\usepackage{graphicx} % Required for the inclusion of images
\usepackage{pdfpages}
\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements
\usepackage[export]{adjustbox}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[margin=1.5in]{geometry}
\usepackage[italian]{babel}

\setlength\parindent{0pt} % Removes all indentation from paragraphs
\setcounter{secnumdepth}{0}% disables section numbering

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Programmazione di Reti \\ Assignment 2} % Title

\author{Paolo Baldini \and Marco Meluzzi} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\newpage

\tableofcontents

\newpage

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Task 1}
Il simulatore fornito come punto di partenza ha lo scopo di riprodurre il comportamento 
di una rete non affidabile, vale a dire che i pacchetti possono andare persi ed essere 
corrotti. Nello specifico, viene preso in considerazione solo il caso unidirezionale, in 
cui l’entità \textit{A}, detta \textit{Sender}, invia pacchetti all’entità \textit{B}, 
detta \textit{Receiver}, che risponde inviando pacchetti di acknowledgment per 
comunicare l’avvenuta ricezione.\newline
Preliminarmente, vengono inizializzate le variabili necessarie all’implementazione del 
protocollo, tramite la chiamata alle routine {\fontfamily{cmtt}\selectfont A\_init()} e {\fontfamily{cmtt}\selectfont B\_init()} rispettivamente per il \textit{Sender} e per il \textit{Receiver}.\newline
Dopodiché, non appena il layer 5 del \textit{Sender} ha dei messaggi da inviare ai 
livelli sottostanti, viene chiamata la routine {\fontfamily{cmtt}\selectfont A\_output()}, che si colloca al 
layer 4 di trasporto e pertanto si occupa di instaurare una comunicazione logica tra il 
\textit{Sender A} e il \textit{Receiver B}, assicurandosi che i pacchetti vengano 
ricevuti correttamente e nella giusta sequenza dal livello applicativo del 
\textit{Receiver}.\newline
A questo punto, con {\fontfamily{cmtt}\selectfont starttimer()} viene fatto partire anche il timer per il 
pacchetto, che tramite la routine {\fontfamily{cmtt}\selectfont tolayer3()} viene passato al layer 3. Quando il pacchetto arriva presso il \textit{Receiver}, viene chiamata la {\fontfamily{cmtt}\selectfont B\_input()} che, controllata l’integrità, invia il pacchetto al layer 5 tramite {\fontfamily{cmtt}\selectfont tolayer5()} e contestualmente invia l’ACK al \textit{Sender} sfruttando ancora una volta 
{\fontfamily{cmtt}\selectfont tolayer3()}.\newline
Quando \textit{A} riceve l’acknowledgment da \textit{B} viene chiamata 
{\fontfamily{cmtt}\selectfont A\_input()}, che controlla la correttezza dell’ACK e in caso positivo ferma il 
timer tramite {\fontfamily{cmtt}\selectfont stoptimer()}. Nel caso l’ACK non venga ricevuto entro lo scadere 
del timer, interviene {\fontfamily{cmtt}\selectfont A\_timeinterrupt()}, che si occupa della ritrasmissione 
del pacchetto e quindi dell’avvio di un nuovo timer.
\newline \newline
Il comportamento sopra descritto può essere sintetizzato con lo schema seguente:
\begin{center}
\centering { \includegraphics[scale=0.60]{res/overall_behaviour.pdf} \par }
\end{center}
\newpage

%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Task 2}
Per realizzare il protocollo \textit{Stop-and-Wait} richiesto ci si è avvalsi di 
strutture  dati e variabili aggiuntive, allo scopo di facilitarne l’implementazione.\\
In particolare, è stata introdotta la {\fontfamily{cmtt}\selectfont struct queue} per gestire la coda dei 
messaggi che arrivano dal layer 5 del \textit{Sender} e che attendono di essere inviati 
ai livelli sottostanti e quindi al \textit{Receiver}. Per comodità si è scelto di 
rendere globali le due variabili che rappresentano il primo e l’ultimo nodo della coda, 
piuttosto che aggiungere overhead non necessario alla coda stessa.\\
Sono state poi aggiunte alcune variabili di utilità per il \textit{Sender} e il 
\textit{Receiver}, nello specifico per tenere traccia dello stato attuale dell’entità 
\textit{A} ({\fontfamily{cmtt}\selectfont sender\_state}), del numero di sequenza dell’ultimo pacchetto inviato dal \textit{Sender} ({\fontfamily{cmtt}\selectfont sender\_seq\_num}), di quello che si aspetta il 
\textit{Receiver} ({\fontfamily{cmtt}\selectfont receiver\_seq\_num}) e una {\fontfamily{cmtt}\selectfont struct pkt} che rappresenta l’ultimo pacchetto inviato dal \textit{Sender} ({\fontfamily{cmtt}\selectfont last\_sender\_packet}).\\
Seguendo le definizioni già fornite, sono state aggiunte le costanti per la gestione 
degli stati del \textit{Sender} ({\fontfamily{cmtt}\selectfont WAIT\_MESSAGE} e {\fontfamily{cmtt}\selectfont WAIT\_ACK}) e la costante {\fontfamily{cmtt}\selectfont SECOND\_SEQNO} per il secondo numero di sequenza utilizzato per garantire l’ordine dei pacchetti: trattandosi di un protocollo \textit{Stop-and-Wait}, infatti, ad ogni istante solo un pacchetto risulta essere in transito dal 
\textit{Sender} al \textit{Receiver}, quindi bastano due soli numeri di sequenza.\\
Infine, le routine di utilità che sono state aggiunte sono {\fontfamily{cmtt}\selectfont enqueue(struct msg)}e {\fontfamily{cmtt}\selectfont dequeue()} rispettivamente per l’accodamento e l’eliminazione di un pacchetto dalla coda, e {\fontfamily{cmtt}\selectfont checksum(struct pkt)}, che calcola il campo checksum del singolo pacchetto seguendo un approccio simile a quello utilizzato per TCP, ovvero sommando carattere per carattere il payload del pacchetto assieme al numero di sequenza e di ACK dello stesso.
\newline \newline
Le routine implementate sono le seguenti:
\paragraph{{\fontfamily{cmtt}\selectfont A\_output(struct msg)}\\}
Quando il layer 5 invia un pacchetto al layer 4, viene chiamata questa routine: se il 
\textit{Sender} non è pronto ad accettare un pacchetto dal livello applicativo, ad esempio perché 
sta aspettando l’acknowledgment per l’ultimo pacchetto inviato, viene aggiunto alla coda. 
Se invece può procedere effettivamente con l’invio del pacchetto, ne calcola il checksum 
e chiama la routine {\fontfamily{cmtt}\selectfont tolayer3()} per indirizzarlo al livello sottostante. Contestualmente, 
viene fatto partire il timer.

\paragraph{{\fontfamily{cmtt}\selectfont A\_input(struct pkt)}\\}
Questa è la routine che viene chiamata dal layer 3 quando il \textit{Sender} riceve un pacchetto 
dal \textit{Receiver}. Considerando solo il caso unidirezionale, l’unico pacchetto che \textit{A} si aspetta 
da \textit{B} è l’acknowledgment per l’ultimo pacchetto inviato. Quindi, se il \textit{Sender} non è in 
attesa di un ACK, significa che ha ricevuto un acknowledgment duplicato e lo scarta, 
stessa cosa se riceve un pacchetto con campo {\fontfamily{cmtt}\selectfont checksum()} o {\fontfamily{cmtt}\selectfont acknum()} errato. Solo in caso contrario procede fermando il timer, aggiornando il numero di sequenza del prossimo pacchetto da inviare, aggiornando lo stato del \textit{Sender} ed eliminando il pacchetto per il quale si è appena ricevuto l’ACK dalla coda.

\paragraph{{\fontfamily{cmtt}\selectfont A\_timeinterrupt(void)}\\}
Viene invocata quando il timer per uno specifico pacchetto scade, al che viene 
ritrasmesso al layer 3 e fatto ripartire il timer.

\paragraph{{\fontfamily{cmtt}\selectfont A\_init(void)}\\}
Si occupa dell’inizializzazione delle variabili che sono state aggiunte per gestire lo 
stato del \textit{Sender} e i numeri di sequenza dei pacchetti da esso inviati.

\paragraph{{\fontfamily{cmtt}\selectfont B\_input(struct pkt)}\\}
Viene chiamata quando un pacchetto giunge al \textit{Receiver}. Se il pacchetto è arrivato integro, 
procede con l’invio dell’ACK al \textit{Sender} e all’inoltro del pacchetto al layer 5. 
Contestualmente viene aggiornato il numero di ACK e di sequenza. Se quest’ultimo non 
corrisponde con quello atteso, viene inviato indietro solamente l’ACK del precedente 
pacchetto ricevuto, senza inviare il pacchetto al livello applicativo, dato che non è 
quello che si aspetta.

\paragraph{{\fontfamily{cmtt}\selectfont B\_init()}\\}
Si occupa dell’inizializzazione della variabile per gestire il numero di sequenza lato \textit{Receiver}.

\newpage

%----------------------------------------------------------------------------------------
%   SECTION 3
%----------------------------------------------------------------------------------------

\section{Macchina a stati finiti - Receiver side}
\begin{center}
\centering { \includegraphics[scale=0.65]{res/fsm_receiver.pdf} \par }
\end{center}
\newpage

%----------------------------------------------------------------------------------------
%   SECTION 4
%----------------------------------------------------------------------------------------

\section{Output Test}
\subsection{Case 1}
\begin{center}
\centering { \includegraphics[width=\textwidth, height=.46\textheight]{res/out01.pdf} \par }
\centering { \includegraphics[width=\textwidth, height=.46\textheight]{res/out02.pdf} \par }
\end{center}

%----------------------------------------------------------------------------------------
%   SECTION 5
%----------------------------------------------------------------------------------------

\begin{center}
\includegraphics[page=1, width=\textwidth, height=.92\textheight]{res/case1.pdf}
\includepdf[pages={2-}, width=\textwidth, pagecommand={}]{res/case1.pdf}
\end{center}
\newpage

%----------------------------------------------------------------------------------------
%   SECTION 6
%----------------------------------------------------------------------------------------

\subsection{Case 2}
\begin{center}
\includegraphics[page=1, width=\textwidth, height=.9\textheight]{res/case2.pdf}
\includepdf[pages={2-}, width=\textwidth, pagecommand={}]{res/case2.pdf}
\end{center}

%----------------------------------------------------------------------------------------

\end{document}