%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

% !TeX spellcheck = it
\documentclass{article}

\usepackage{siunitx} % Provides the \SI{}{} and \si{} command for typesetting SI units
\usepackage{graphicx} % Required for the inclusion of images
\usepackage{pdfpages}
\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements
\usepackage[export]{adjustbox}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[margin=1.5in]{geometry}
\usepackage[italian]{babel}

\setlength\parindent{0pt} % Removes all indentation from paragraphs
\setcounter{secnumdepth}{0}% disables section numbering

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)
\newenvironment{simil_console_font}{\fontfamily{pcr}\selectfont}{\par}

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Programmazione di Reti \\ Assignment 3} % Title

\author{Paolo Baldini \and Marco Meluzzi} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\newpage

\tableofcontents

\newpage

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Task 1}
\paragraph{}
Partendo dal simulatore di rete utilizzato per il secondo
assignment, l’obiettivo è quello di simulare il comportamento del
protocollo \textit{Selective-Repeat}, considerando solamente il caso di
trasferimento unidirezionale dei dati dal \textit{Sender} al \textit{Receiver}, anche
se quest’ultimo deve comunque essere in grado di inviare gli
acknowledgment per i pacchetti di dati ricevuti dal \textit{Sender}. Per
rendere possibile ciò, in particolare per garantire l’affidabilità del
protocollo, dovranno essere previsti due buffer, uno lato \textit{Sender},
che raccoglierà i pacchetti che gli arrivano dal livello applicativo
dell’entità \textit{A}, e uno lato \textit{Receiver}. In questo modo, i pacchetti fuori
sequenza non verranno scartati, bensì accodati in attesa di poter
essere inviati al livello applicativo dell’entità \textit{B} nel giusto ordine.\\
Tuttavia, a differenza del protocollo \textit{Selective-Repeat} tradizionale,
l’implementazione richiesta dovrà fare uso di ACK cumulativi, tipici
del protocollo \textit{Go-Back-n}.

\paragraph{}
Per realizzare il protocollo ibrido richiesto, ci si è avvalsi di
variabili aggiuntive per gestire le informazioni rigurdanti il \textit{Sender},
il \textit{Receiver} e le statistiche della simulazione, e di funzioni
supplementari allo scopo di facilitare l’implementazione. Nello
specifico:
\begin{description}
    \item[\fontfamily{cmtt}\selectfont bool\_t checkSeqNumValidity(int base, int p)]: controlla se il
        numero di sequenza del pacchetto ricevuto sia all’interno
        della finestra di ricezione.
    \item[\fontfamily{cmtt}\selectfont int checksum(struct pkt packet)]: calcola il campo checksum
        del pacchetto utilizzando un approccio simile a quello usato
        per TCP, vale a dire sommando carattere per carattere il
        payload assieme al numero di sequenza e di ACK del
        pacchetto.
    \item[\fontfamily{cmtt}\selectfont int acknowledgment(int seqnum)]: dato il numero di sequenza 
        del pacchetto arrivato al \textit{Receiver}, calcola l’ACK (cumulativo) corretto da
        inviare al \textit{Sender} scorrendo tutti i sequence number dei
        pacchetti contenuti nel buffer di ricezione, fino a trovare l'ultimo della sequenza (non 
        l'ultimo ricevuto ma l'ultimo in ordine).
    \item[\fontfamily{cmtt}\selectfont struct pkt create\_packet(int seq\_num, int ack\_num, char
        data[20\rbrack)]: compone il pacchetto con le informazioni che le vengono passate come 
        argomento.
    \item[\fontfamily{cmtt}\selectfont void packetsToLayer5(int seqnum)]: quando viene chiamata dal
        \textit{Receiver}, invia a livello applicativo tutti i pacchetti contenuti
        nel suo buffer fino a quello con numero di sequenza {\fontfamily{cmtt}\selectfont seqnum}.
    \item[\fontfamily{cmtt}\selectfont void printStatistics()]: stampa le statistiche inerenti le
        simulazioni eseguite, con attenzione al numero di messaggi
        arrivati al \textit{Sender}, al numero di messaggi inviati dal \textit{Sender}, al
        numero di pacchetti ritrasmessi, al numero di ACK corretti
        ricevuti e al numero di pacchetti corrotti ricevuti,
        considerando sia i pacchetti dati che gli acknowledgment.
\end{description}
Le routine implementate sono le seguenti:
\paragraph{\fontfamily{cmtt}\selectfont A\_output(struct msg)\\}
Questa routine viene chiamata quando, presso il \textit{Sender}, il layer 5
invia un pacchetto al layer 4 e il messaggio corrispondente viene
accodato in un buffer. Se il sequence number rientra all’interno
della finestra, si procede alla composizione e quindi all’invio del
pacchetto al livello sottostante mediante la chiamata alla funzione
{\fontfamily{cmtt}\selectfont tolayer3()}. Contestualmente, se il pacchetto inviato è il primo
della finestra, viene fatto partire il timer. Nel caso in cui il 
buffer di invio sia pieno, l’applicazione viene fatta terminare.

\paragraph{\fontfamily{cmtt}\selectfont A\_input(struct pkt)\\}
Questa è la routine che viene chiamata dal layer 3 quando il
\textit{Sender} riceve un pacchetto dal \textit{Receiver}. Se il campo checksum
risulta corrotto, il pacchetto viene scartato. Nel caso opposto, e se
il pacchetto ricevuto (un ACK) ricade all’interno della finestra di
invio, viene fermato il timer e quindi calcolata la nuova posizione
verso cui si deve spostare la finestra di invio. Se sono disponibili
dei messaggi ancora da inviare, questi vengono composti con le
informazioni pertinenti, aggiunti al buffer di invio e inviati al layer
3. Ogni volta che ci sono altri pacchetti da inviare, viene fatto
ripartire il timer.

\paragraph{\fontfamily{cmtt}\selectfont A\_timeinterrupt(void)\\}
Viene invocata quando il timer scade, al che vengono ritrasmessi al
layer 3 tutti i pacchetti della finestra che ancora non hanno
ricevuto l’acknowledgment. Infine, il timer viene fatto ripartire.

\paragraph{\fontfamily{cmtt}\selectfont A\_init(void)\\}
Si occupa dell’inizializzazione delle variabili che sono state
aggiunte per gestire le informazioni di interesse per il \textit{Sender}.

\paragraph{\fontfamily{cmtt}\selectfont B\_input(struct pkt)\\}
Viene chiamata quando un pacchetto giunge al \textit{Receiver}. Nel caso
in cui il pacchetto sia corrotto, viene inviato nuovamente l’ultimo
ACK. In caso contrario, se viene ricevuto un pacchetto fuori
sequenza, questo viene accodato nel buffer in attesa di poter
essere inviato al layer 5 e viene anche in questo caso inviato un
ACK duplicato. Se invece ad essere ricevuto è il pacchetto con il
numero di sequenza atteso, si procede a calcolare il numero del
nuovo acknowledgment tramite l'omonima funzione. L’ACK così trovato 
viene inviato al \textit{Sender} e contestualmente il \textit{Receiver} invia al 
layer 5 tutti i pacchetti nel buffer di ricezione non ancora inviati 
e in ordine.

\paragraph{\fontfamily{cmtt}\selectfont B\_init()\\}
Si occupa dell’inizializzazione delle variabili che sono state
aggiunte per gestire le informazioni di interesse per il \textit{Receiver}.

\newpage

%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Macchina a stati finiti}

\subsection{Sender side}
\begin{figure}[!hbt]
    \begin{center}
        \includegraphics[width=\textwidth]{res/fsm-sender.pdf}
    \end{center}
\end{figure}

\subsection{Receiver side}
\begin{figure}[!hbt]
    \begin{center}
        \includegraphics[scale=0.917]{res/fsm-receiver.pdf}
    \end{center}
\end{figure}

\newpage

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------

\section{Test scenario}

\subsection{Case 1}
\begin{center}
    \includegraphics[page=1, width=\textwidth, height=.88\textheight]{res/case1.pdf}
    \includepdf[pages={2-}, width=\textwidth, pagecommand={}]{res/case1.pdf}    
\end{center}
\newpage

\subsection{Case 2}
\begin{center}
    \includegraphics[page=1, width=\textwidth, height=.9\textheight]{res/case2.pdf}
    \includepdf[pages={2-}, width=\textwidth, pagecommand={}]{res/case2.pdf}    
\end{center}
\newpage

\subsection{Case 3}
\begin{center}
    \includegraphics[page=1, width=\textwidth, height=.9\textheight]{res/case3.pdf}
    \includepdf[pages={2-}, width=\textwidth, pagecommand={}]{res/case3.pdf}    
\end{center}
\newpage

%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Considerazioni sull'uso di un timeout di ritrasmissione statico}
\paragraph{}
Nell'implementazione sviluppata si è fatto uso di un valore di timeout statico per la 
ritrasmissione dei pacchetti, scelto arbitrariamente dall'utente prima di fare partire 
la simulazione. Questa scelta, benché immediata, comporta sicuramente degli svantaggi e 
non rispecchia la reale implementazione dei meccanismi di gestione della ritrasmissione. 
Chiamato \textit{RTT} il tempo di andata e ritorno, ovvero il tempo trascorso da quando 
si invia un pacchetto a quando se ne riceve l'acknowledgment, è chiaro che il timeout 
scelto per la ritrasmissione deve essere più grande, altrimenti ci sarebbero delle 
ritrasmissioni inutili. D'altra parte, scegliere un timeout troppo prolungato potrebbe 
portare ad attendere una quantità di tempo eccessiva e rendere la trasmissione lenta e 
poco efficiente. E' quindi importante scegliere un buon compromesso tenendo conto di 
questi due aspetti.\\
Nelle situazioni reali, TCP ovvia a queste problematiche facendo uso di timeout di 
ritrasmissione dinamici, che variano in base allo stato attuale della rete. Nello 
specifico, per ogni istante di tempo viene valutato il \textit{RTT}, che chiameremo 
\textit{SampleRTT,} per uno solo dei segmenti trasmessi e per cui non si è ancora 
ricevuto un ACK. Dato che le misurazioni variano da pacchetto a pacchetto in base alla 
congestione attuale della rete, i singoli valori rilevati potrebbero fluttuare anche 
considerevolmente, per questo si effettua una media di valori di \textit{SampleRTT}, detta 
\textit{EstimatedRTT}, che consiste in una combinazione ponderata del precedente valore 
registrato e del nuovo valore di \textit{SampleRTT}: in questo modo, viene attribuita 
maggiore rilevanza ai campionamenti recenti rispetto a quelli vecchi, sempre al fine di 
dare una stima affidabile della situazione corrente della rete. La formula con la quale 
viene aggiornato \textit{EstimatedRTT} ad ogni passo è la seguente:
{\fontfamily{cmtt}\selectfont \[EstimatedRTT = (1 - \alpha) * EstimatedRTT + \alpha * SampleRTT\]}
Inoltre, TCP tiene in considerazione anche la variabilità dei \textit{RTT} registrati, 
stimando quanto \textit{SampleRTT} si discosta da \textit{EstimatedRTT}, mediante la 
formula:
{\fontfamily{cmtt}\selectfont \[DevRTT = (1 - \beta) * DevRTT + \beta * | SampleRTT - EstimatedRTT | \]}
Quindi, la stima finale del timeout di ritrasmissione, tenendo conto del margine di 
fluttuazione dei valori, risulta essere:
{\fontfamily{cmtt}\selectfont \[TimeoutInterval = EstimatedRTT + 4 * DevRTT\]}




\newpage
%----------------------------------------------------------------------------------------

\end{document}