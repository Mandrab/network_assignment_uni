/****************************************************************************************

Copyright 2018 Paolo Baldini, Marco Meluzzi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

****************************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/file.h>
#include <sys/types.h>
#include <netinet/in.h>

/* ******************************************************************
   This code should be used for unidirectional data transfer protocols 
   (from A to B)
   Network properties:
   - one way network delay averages five time units (longer if there
     are other messages in the channel for Pipelined ARQ), but can be larger
   - packets can be corrupted (either the header or the data portion)
     or lost, according to user-defined probabilities
   - packets will be delivered in the order in which they were sent
     (although some can be lost).
**********************************************************************/

/* a "msg" is the data unit passed from layer 5 (teachers code) to layer  */
/* 4 (students' code).  It contains the data (characters) to be delivered */
/* to layer 5 via the students transport level protocol entities.         */
struct msg {
  char data[20];
};

/* a packet is the data unit passed from layer 4 (students code) to layer */
/* 3 (teachers code).  Note the pre-defined packet structure, which all   */
/* students must follow. */
struct pkt {
  int seqnum;
  int acknum;
  int checksum;
  char payload[20];
};

/*- Your Definitions 
  ---------------------------------------------------------------------------*/

#define MAX_BUFFER_SIZE 50

#define PAYLOAD_COPY(INTO, FROM)  for (int i = 0; i < 20; i++)    \
                                    INTO[i] = FROM[i];

#define PAYLOAD_PRINT(DATA)   for (int i = 0; i < 20; i++) \
                                printf("%c", DATA[i]);

typedef enum BOOL {
  false, true
} bool_t;

int sender_base_sq_num;                 // first sequence number of window
int sender_next_sq_num;                 // last sequence number within window
int sender_next_msg_id;                 // message index that keeps track of the next message to be transferred from the buffer to the window
int sender_msg_count;                   // message count
int sender_buffer_size;                 // current buffer dimension
struct msg *sender_msg_buffer;          // message buffer
struct pkt *sender_packet_buffer;       // packet buffer

int receiver_expect_sq_num;             // expected sequence number
int receiver_last_ack_num;              // last acknowledgment number
int receiver_is_first_iteration;        // utility variable
bool_t *receiver_packet_sent;           // utilty array to trace packets already sent to layer 5
int receiver_last_sq_num;               // last sequence number correctly received
struct pkt *receiver_packet_buffer;     // buffer of arrived packets

/* Statistics */
int sender_pkt_count;                   // count of packets sent by Sender
int receiver_corrupted_pkt_count;       // count of corrupted packets received by Receiver
int retransmitted_pkt_count;            // retransmitted packets counter
int succesfully_transferred_pkt_count;  // communicated packets (correct acks) counter

/* Please use the following values in your program */

#define   A    0
#define   B    1
#define   FIRST_SEQNO   0

/*- Declarations ------------------------------------------------------------*/
void	restart_rxmt_timer(void);
void	tolayer3(int AorB, struct pkt packet);
void	tolayer5(char datasent[20]);

void	starttimer(int AorB, double increment);
void	stoptimer(int AorB);

/* WINDOW_SIZE, RXMT_TIMEOUT and TRACE are inputs to the program;
   We have set an appropriate value for LIMIT_SEQNO.
   You do not have to concern but you have to use these variables in your 
   routines --------------------------------------------------------------*/

extern int WINDOW_SIZE;      // size of the window
extern int LIMIT_SEQNO;      // when sequence number reaches this value,                                     // it wraps around
extern double RXMT_TIMEOUT;  // retransmission timeout
extern int TRACE;            // trace level, for your debug purpose
extern double time_now;      // simulation time, for your debug purpose

/********* YOU MAY ADD SOME ROUTINES HERE ********/

/* Validity check of the sequence number (i.e. it is within the window) */
bool_t checkSeqNumValidity(int base, int p) {
  return (p >= base && p < base + WINDOW_SIZE) || (p < base - WINDOW_SIZE);
}

/* The routine that calculates the checksum according to TCP approach */
int checksum(struct pkt packet) {
	int checksum = 0;
	checksum += packet.seqnum;
	checksum += packet.acknum;
	for (int i = 0; i < 20; i++) {
    checksum += packet.payload[i];
  }
	return checksum;
}

/* The routine that calculates the correct ACK */
int acknowledgment(int seqnum) {
  receiver_is_first_iteration++;
  for (int i = seqnum % WINDOW_SIZE; receiver_packet_buffer[i].seqnum == seqnum; i = ((++seqnum) % LIMIT_SEQNO) % WINDOW_SIZE) {
    receiver_is_first_iteration++;
  }
  return seqnum;
}

/* The routine that creates a packet with the given sequence number, ACK number and data */
struct pkt create_packet(int seq_num, int ack_num, char data[20]) {
	struct pkt new_packet;
  new_packet.seqnum = seq_num;
  new_packet.acknum = ack_num;
  char* str = data != NULL ? data : (char[20]){};
  PAYLOAD_COPY(new_packet.payload, str);
  new_packet.checksum = checksum(new_packet);
	return new_packet;
}

/* The routine that sends packets to layer 5 */
void packetsToLayer5(int seqnum) {
  for (int i = seqnum % WINDOW_SIZE; receiver_packet_buffer[i].seqnum == seqnum; i = ((++seqnum) % LIMIT_SEQNO) % WINDOW_SIZE) {
    if (!receiver_packet_sent[i]) {
			tolayer5(receiver_packet_buffer[i].payload);
			receiver_packet_sent[i] = true;
		}
  }
}

/* Printing of network statistics */
void printStatistics() {
	printf("NETWORK STATISTICS:\n"\
    "Number of messages arrived to A: %d\n" \
    "Number of packets sent from A: %d\n" \
	  "Number of packet retransmissions: %d\n"  \
	  "Number of correct acks received: %d\n" \
	  "Number of corrupted packets (data and ACK): %d\n\n", 
    sender_msg_count, sender_pkt_count, retransmitted_pkt_count, 
    succesfully_transferred_pkt_count, receiver_corrupted_pkt_count);
}

/********* STUDENTS WRITE THE NEXT SIX ROUTINES *********/

/* called from layer 5, passed the data to be sent to other side */
void A_output (message) struct msg message;
{
  printf("A_output(): sender received a message from the application layer. Enqueued message: ");
  PAYLOAD_PRINT(message.data); /* printing the packet payload */

  sender_msg_buffer[sender_msg_count] = message;
	sender_msg_count++;
	sender_buffer_size++;

  if (sender_buffer_size > MAX_BUFFER_SIZE) {
		printf("A_output(): Buffer full. Application aborted.\n");
		exit(1);
	}

	if (checkSeqNumValidity(sender_base_sq_num, sender_next_sq_num)) { /* if next sequence number is within the window, the message is sent */
		struct pkt packet = create_packet(sender_next_sq_num, 0, sender_msg_buffer[sender_next_msg_id].data);

		sender_packet_buffer[sender_next_sq_num] = packet;

		printf("A_output(): packet with seqnum %d sent: ", packet.seqnum);
    PAYLOAD_PRINT(packet.payload); /* printing the packet payload */

		tolayer3(A, packet);

		sender_pkt_count++;

		if (sender_next_sq_num == sender_base_sq_num) { /* if sent packet was the first of the window, timer starts */
      starttimer(A, RXMT_TIMEOUT);
    }

		sender_next_sq_num = (sender_next_sq_num + 1) % LIMIT_SEQNO; /* setting of the sequence number of the next packet to be sent */

		sender_next_msg_id++; /* the next message to be transferred from the buffer to the window */
	}
}

/* called from layer 3, when a packet arrives for layer 4 */
void A_input(packet) struct pkt packet;
{	
  printf("A_input(): sender received ACK %d with seqnum %d from receiver\n", packet.acknum, packet.seqnum);

  if ((packet.checksum == checksum(packet)) && checkSeqNumValidity(sender_base_sq_num, packet.acknum)) {
    printf("A_input(): correct ACK.\n");

    stoptimer(A);

    int new_pos = (packet.acknum >= sender_base_sq_num) ? packet.acknum - sender_base_sq_num : packet.acknum - sender_base_sq_num + LIMIT_SEQNO;

    sender_base_sq_num = (packet.acknum + 1) % LIMIT_SEQNO; /* updating sender base number */

    for (int i = 0; i <= new_pos; i++) { /* checking the new positions available in the window */
      if (sender_next_msg_id < sender_msg_count) {
        struct pkt data_packet = create_packet(sender_next_sq_num, 0, sender_msg_buffer[sender_next_msg_id++].data); /* copying the message stored in the buffer in the packet payload to be sent */

		    sender_packet_buffer[sender_next_sq_num] = data_packet;

        printf("A_input(): packet with seqnum %d sent: ", data_packet.seqnum);
        PAYLOAD_PRINT(data_packet.payload); /* printing the packet payload */

		    tolayer3(A, data_packet);

        sender_pkt_count++;

        sender_next_sq_num = (sender_next_sq_num + 1) % LIMIT_SEQNO; /* setting of the sequence number of the next packet to be sent */
      }
      succesfully_transferred_pkt_count++;
			sender_buffer_size--;
    }
    if (sender_next_sq_num != sender_base_sq_num) { /* if there are still packets to send, timer starts */
      starttimer(A, RXMT_TIMEOUT);
    }
  } else {
    if (packet.checksum != checksum(packet)) {
			receiver_corrupted_pkt_count++;
			printf("A_input(): corrupted ACK.\n");
		}
		printf("A_input(): expected ACK: %d\n", sender_base_sq_num);
  }
}

/* called when A's timer goes off */
void A_timerinterrupt (void)
{
  struct pkt packet;
  int sq_num = sender_base_sq_num;

  while (sq_num != sender_next_sq_num) { /* resending packets that did not receive the ACK */
    packet = sender_packet_buffer[sq_num];
    printf("A_timerinterrupt(): packet with seqnum %d re-sent\n", packet.seqnum);

    tolayer3(A, packet);

		sender_pkt_count++;
		retransmitted_pkt_count++;

		sq_num = (sq_num + 1) % LIMIT_SEQNO;
  }

  starttimer(A, RXMT_TIMEOUT);
} 

/* the following routine will be called once (only) before any other */
/* entity A routines are called. You can use it to do any initialization */
void A_init (void)
{
  sender_base_sq_num = FIRST_SEQNO;
	sender_next_sq_num = FIRST_SEQNO;
	sender_next_msg_id = 0;
	sender_msg_count = 0;
  sender_buffer_size = 0;
	sender_msg_buffer = (struct msg *)malloc(sizeof(struct msg) * MAX_BUFFER_SIZE);
	sender_packet_buffer = (struct pkt *)malloc(sizeof(struct pkt) * LIMIT_SEQNO);
	
	sender_pkt_count = 0;
	receiver_corrupted_pkt_count = 0;
	retransmitted_pkt_count = 0;
	succesfully_transferred_pkt_count = 0;
} 

// Called from layer 3, when a packet arrives for layer 4 at B
void B_input (packet) struct pkt packet;
{
  struct pkt new_packet;
  
  printf("B_input(): receiver received packet with seqnum %d and ACK %d from sender. Message: ", packet.seqnum, packet.acknum);

	if (packet.checksum != checksum(packet)) { /* packet corrupt */
		receiver_corrupted_pkt_count++;
		new_packet = create_packet(0, receiver_last_ack_num, NULL);
	}	else {
    if (!(packet.seqnum >= receiver_expect_sq_num && packet.seqnum < receiver_expect_sq_num + WINDOW_SIZE) &&
      !(packet.seqnum < receiver_expect_sq_num - WINDOW_SIZE)){
      new_packet = create_packet(0, receiver_last_ack_num, NULL);
    } else {
      receiver_packet_sent[(packet.seqnum % WINDOW_SIZE)] = 0; /* the packet is not yet sent to the application layer, */
      receiver_packet_buffer[(packet.seqnum % WINDOW_SIZE)].seqnum = packet.seqnum; /* but it is stored in the buffer waiting to be sent */
      PAYLOAD_COPY(receiver_packet_buffer[packet.seqnum % WINDOW_SIZE].payload, packet.payload);
      int ack = (acknowledgment(receiver_expect_sq_num)) % WINDOW_SIZE;

      if (!(packet.seqnum < receiver_expect_sq_num - WINDOW_SIZE) && (receiver_is_first_iteration == 1))
        new_packet = create_packet(0, receiver_last_ack_num, NULL);
      else {
        if (!(packet.seqnum < receiver_expect_sq_num - WINDOW_SIZE) && !(receiver_is_first_iteration == 1))
          ack = ack > 0 ? ack - 1 : WINDOW_SIZE - 1;
        packetsToLayer5((receiver_last_sq_num + 1) % LIMIT_SEQNO);

        new_packet = create_packet(0, receiver_packet_buffer[ack].seqnum, NULL);

        receiver_last_sq_num = receiver_last_ack_num = new_packet.acknum;
        receiver_expect_sq_num = (new_packet.acknum + 1) % LIMIT_SEQNO; /* updating expected sequence number */
      }
    }
    receiver_is_first_iteration = 0;
  }

	/* Sending packet to the network */
  printf("B_input(): sending ACK %d to A\n", new_packet.acknum);
  tolayer3(B, new_packet);
}

/* the following rouytine will be called once (only) before any other */
/* entity B routines are called. You can use it to do any initialization */
void
B_init (void)
{
  receiver_expect_sq_num = FIRST_SEQNO;
	receiver_last_ack_num = FIRST_SEQNO - 1 < 0 ? LIMIT_SEQNO - 1 : FIRST_SEQNO - 1;
	receiver_is_first_iteration = 0;
  receiver_packet_sent = malloc(sizeof(int) * WINDOW_SIZE);
  receiver_last_sq_num = -1;
  receiver_packet_buffer = (struct pkt *)malloc(sizeof(struct pkt) * WINDOW_SIZE);
	for (int i = 0; i < WINDOW_SIZE; i++) {
		receiver_packet_buffer[i].seqnum = -1;
		receiver_packet_sent[i] = 0;
	}
} 

/*****************************************************************
***************** NETWORK EMULATION CODE STARTS BELOW ***********
The code below emulates the layer 3 and below network environment:
  - emulates the tranmission and delivery (possibly with bit-level corruption
    and packet loss) of packets across the layer 3/4 interface
  - handles the starting/stopping of a timer, and generates timer
    interrupts (resulting in calling students timer handler).
  - generates message to be sent (passed from later 5 to 4)

THERE IS NOT REASON THAT ANY STUDENT SHOULD HAVE TO READ OR UNDERSTAND
THE CODE BELOW.  YOU SHOLD NOT TOUCH, OR REFERENCE (in your code) ANY
OF THE DATA STRUCTURES BELOW.  If you're interested in how I designed
the emulator, you're welcome to look at the code - but again, you should have
to, and you defeinitely should not have to modify
******************************************************************/


struct event {
  double evtime;           /* event time */
  int evtype;             /* event type code */
  int eventity;           /* entity where event occurs */
  struct pkt *pktptr;     /* ptr to packet (if any) assoc w/ this event */
  struct event *prev;
  struct event *next;
};
struct event *evlist = NULL;   /* the event list */

/* Advance declarations. */
void init(void);
void generate_next_arrival(void);
void insertevent(struct event *p);


/* possible events: */
#define  TIMER_INTERRUPT 0
#define  FROM_LAYER5     1
#define  FROM_LAYER3     2

#define  OFF             0
#define  ON              1


int TRACE = 0;              /* for debugging purpose */
int fileoutput; 
double time_now = 0.000;
int WINDOW_SIZE;
int LIMIT_SEQNO;
double RXMT_TIMEOUT;
double lossprob;            /* probability that a packet is dropped  */
double corruptprob;         /* probability that one bit is packet is flipped */
double lambda;              /* arrival rate of messages from layer 5 */
int   ntolayer3;           /* number sent into layer 3 */
int   nlost;               /* number lost in media */
int ncorrupt;              /* number corrupted by media*/
int nsim = 0;
int nsimmax = 0;
unsigned int seed[5];         /* seed used in the pseudo-random generator */

int
main(int argc, char **argv)
{
  struct event *eventptr;
  struct msg  msg2give;
  struct pkt  pkt2give;

  int i,j;

  init();
  A_init();
  B_init();

  while (1) {
    eventptr = evlist;            /* get next event to simulate */
    if (eventptr==NULL)
      goto terminate;
    evlist = evlist->next;        /* remove this event from event list */
    if (evlist!=NULL)
      evlist->prev=NULL;
    if (TRACE>=2) {
      printf("\nEVENT time: %f,",eventptr->evtime);
      printf("  type: %d",eventptr->evtype);
      if (eventptr->evtype==0)
        printf(", timerinterrupt  ");
      else if (eventptr->evtype==1)
        printf(", fromlayer5 ");
      else
        printf(", fromlayer3 ");
      printf(" entity: %d\n",eventptr->eventity);
    }
    time_now = eventptr->evtime;    /* update time to next event time */
    if (eventptr->evtype == FROM_LAYER5 ) {
      generate_next_arrival();   /* set up future arrival */
      /* fill in msg to give with string of same letter */
      j = nsim % 26;
      for (i=0;i<20;i++)
        msg2give.data[i]=97+j;
      msg2give.data[19]='\n';
      nsim++;
      if (nsim==nsimmax+1)
        break;
      A_output(msg2give);
    } else if (eventptr->evtype ==  FROM_LAYER3) {
      pkt2give.seqnum = eventptr->pktptr->seqnum;
      pkt2give.acknum = eventptr->pktptr->acknum;
      pkt2give.checksum = eventptr->pktptr->checksum;
      for (i=0;i<20;i++)
        pkt2give.payload[i]=eventptr->pktptr->payload[i];
      if (eventptr->eventity == A)      /* deliver packet by calling */
        A_input(pkt2give);            /* appropriate entity */
      else
        B_input(pkt2give);
      free(eventptr->pktptr);          /* free the memory for packet */
    } else if (eventptr->evtype ==  TIMER_INTERRUPT) {
      A_timerinterrupt();
    } else  {
      printf("INTERNAL PANIC: unknown event type \n");
    }
    free(eventptr);
  }
  terminate:
    printf("Simulator terminated at time %.12f\n",time_now);
    printStatistics();
    return (0);
}


void
init(void)                         /* initialize the simulator */
{
  int i=0;
  printf("----- * ARQ Network Simulator Version 1.1 * ------ \n\n");
  printf("Enter number of messages to simulate: ");
  scanf("%d",&nsimmax);
  printf("Enter packet loss probability [enter 0.0 for no loss]:");
  scanf("%lf",&lossprob);
  printf("Enter packet corruption probability [0.0 for no corruption]:");
  scanf("%lf",&corruptprob);
  printf("Enter average time between messages from sender's layer5 [ > 0.0]:");
  scanf("%lf",&lambda);
  printf("Enter window size [>0]:");
  scanf("%d",&WINDOW_SIZE);
  LIMIT_SEQNO = 2*WINDOW_SIZE;
  printf("Enter retransmission timeout [> 0.0]:");
  scanf("%lf",&RXMT_TIMEOUT);
  printf("Enter trace level:");
  scanf("%d",&TRACE);
  printf("Enter random seed: [>0]:");
  scanf("%d",&seed[0]);
  for (i=1;i<5;i++)
    seed[i]=seed[0]+i;
  fileoutput = open("OutputFile", O_CREAT|O_WRONLY|O_TRUNC,0644);
  if (fileoutput<0) 
    exit(1);
  ntolayer3 = 0;
  nlost = 0;
  ncorrupt = 0;
  time_now=0.0;                /* initialize time to 0.0 */
  generate_next_arrival();     /* initialize event list */
}

/****************************************************************************/
/* mrand(): return a double in range [0,1].  The routine below is used to */
/* isolate all random number generation in one location.  We assume that the*/
/* system-supplied rand() function return an int in therange [0,mmm]        */
/****************************************************************************/
int nextrand(int i)
{
  seed[i] = seed[i]*1103515245+12345;
  return (unsigned int)(seed[i]/65536)%32768;
}

double mrand(int i)
{
  double mmm = 32767;   
  double x;                   /* individual students may need to change mmm */
  x = nextrand(i)/mmm;            /* x should be uniform in [0,1] */
  if (TRACE==0)
    printf("%.16f\n",x);
  return(x);
}


/********************* EVENT HANDLINE ROUTINES *******/
/*  The next set of routines handle the event list   */
/*****************************************************/
void
generate_next_arrival(void)
{
  double x,log(),ceil();
  struct event *evptr;

    
  if (TRACE>2)
    printf("          GENERATE NEXT ARRIVAL: creating new arrival\n");

  x = lambda*mrand(0)*2;  /* x is uniform on [0,2*lambda] */
  /* having mean of lambda        */
  evptr = (struct event *)malloc(sizeof(struct event));
  evptr->evtime =  time_now + x;
  evptr->evtype =  FROM_LAYER5;
  evptr->eventity = A;
  insertevent(evptr);
}

void
insertevent(p)
  struct event *p;
{
  struct event *q,*qold;

  if (TRACE>2) {
    printf("            INSERTEVENT: time is %f\n",time_now);
    printf("            INSERTEVENT: future time will be %f\n",p->evtime);
  }
  q = evlist;     /* q points to header of list in which p struct inserted */
  if (q==NULL) {   /* list is empty */
    evlist=p;
    p->next=NULL;
    p->prev=NULL;
  } else {
    for (qold = q; q !=NULL && p->evtime > q->evtime; q=q->next)
      qold=q;
    if (q==NULL) {   /* end of list */
      qold->next = p;
      p->prev = qold;
      p->next = NULL;
    } else if (q==evlist) { /* front of list */
      p->next=evlist;
      p->prev=NULL;
      p->next->prev=p;
      evlist = p;
    } else {     /* middle of list */
      p->next=q;
      p->prev=q->prev;
      q->prev->next=p;
      q->prev=p;
    }
  }
}

void
printevlist(void)
{
  struct event *q;
  printf("--------------\nEvent List Follows:\n");
  for(q = evlist; q!=NULL; q=q->next) {
    printf("Event time: %f, type: %d entity: %d\n",q->evtime,q->evtype,q->eventity);
  }
  printf("--------------\n");
}



/********************** Student-callable ROUTINES ***********************/

/* called by students routine to cancel a previously-started timer */
void
stoptimer(AorB)
int AorB;  /* A or B is trying to stop timer */
{
  struct event *q /* ,*qold */;
  if (TRACE>2)
    printf("          STOP TIMER: stopping timer at %f\n",time_now);
  for (q=evlist; q!=NULL ; q = q->next)
    if ( (q->evtype==TIMER_INTERRUPT  && q->eventity==AorB) ) {
      /* remove this event */
      if (q->next==NULL && q->prev==NULL)
        evlist=NULL;         /* remove first and only event on list */
      else if (q->next==NULL) /* end of list - there is one in front */
        q->prev->next = NULL;
      else if (q==evlist) { /* front of list - there must be event after */
        q->next->prev=NULL;
        evlist = q->next;
      } else {     /* middle of list */
        q->next->prev = q->prev;
        q->prev->next =  q->next;
      }
      free(q);
      return;
    }
  printf("Warning: unable to cancel your timer. It wasn't running.\n");
}


void
starttimer(AorB,increment)
int AorB;  /* A or B is trying to stop timer */
double increment;
{
  struct event *q;
  struct event *evptr;

  if (TRACE>2)
    printf("          START TIMER: starting timer at %f\n",time_now);
  /* be nice: check to see if timer is already started, if so, then  warn */
  /* for (q=evlist; q!=NULL && q->next!=NULL; q = q->next)  */
  for (q=evlist; q!=NULL ; q = q->next)
    if ( (q->evtype==TIMER_INTERRUPT  && q->eventity==AorB) ) {
      printf("Warning: attempt to start a timer that is already started\n");
      return;
    }

  /* create future event for when timer goes off */
  evptr = (struct event *)malloc(sizeof(struct event));
  evptr->evtime =  time_now + increment;
  evptr->evtype =  TIMER_INTERRUPT;
  evptr->eventity = AorB;
  insertevent(evptr);
}


/************************** TOLAYER3 ***************/
void
tolayer3(AorB,packet)
int AorB;  /* A or B is trying to stop timer */
struct pkt packet;
{
  struct pkt *mypktptr;
  struct event *evptr,*q;
  double lastime, x;
  int i;


  ntolayer3++;

  /* simulate losses: */
  if (mrand(1) < lossprob)  {
    nlost++;
    if (TRACE>0)
      printf("          TOLAYER3: packet being lost\n");
    return;
  }

  /* make a copy of the packet student just gave me since he/she may decide */
  /* to do something with the packet after we return back to him/her */
  mypktptr = (struct pkt *)malloc(sizeof(struct pkt));
  mypktptr->seqnum = packet.seqnum;
  mypktptr->acknum = packet.acknum;
  mypktptr->checksum = packet.checksum;
  for (i=0;i<20;i++)
    mypktptr->payload[i]=packet.payload[i];
  if (TRACE>2)  {
    printf("          TOLAYER3: seq: %d, ack %d, check: %d ", mypktptr->seqnum,
    mypktptr->acknum,  mypktptr->checksum);
  }

  /* create future event for arrival of packet at the other side */
  evptr = (struct event *)malloc(sizeof(struct event));
  evptr->evtype =  FROM_LAYER3;   /* packet will pop out from layer3 */
  evptr->eventity = (AorB+1) % 2; /* event occurs at other entity */
  evptr->pktptr = mypktptr;       /* save ptr to my copy of packet */
  /* finally, compute the arrival time of packet at the other end.
   medium can not reorder, so make sure packet arrives between 1 and 10
   time units after the latest arrival time of packets
   currently in the medium on their way to the destination */
  lastime = time_now;
  for (q=evlist; q!=NULL ; q = q->next)
    if ( (q->evtype==FROM_LAYER3  && q->eventity==evptr->eventity) )
      lastime = q->evtime;
  evptr->evtime =  lastime + 1 + 9*mrand(2);



  /* simulate corruption: */
  if (mrand(3) < corruptprob)  {
    ncorrupt++;
    if ( (x = mrand(4)) < 0.75)
      mypktptr->payload[0]='?';   /* corrupt payload */
    else if (x < 0.875)
      mypktptr->seqnum = 999999;
    else
      mypktptr->acknum = 999999;
    if (TRACE>0)
      printf("          TOLAYER3: packet being corrupted\n");
  }

  if (TRACE>2)
     printf("          TOLAYER3: scheduling arrival on other side\n");
  insertevent(evptr);
}

void
tolayer5(datasent)
  char datasent[20];
{
  write(fileoutput,datasent,20);
}
